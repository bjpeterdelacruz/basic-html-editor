# Basic HTML Editor

## Live Demo

[https://basichtmleditor.azurewebsites.net](https://basichtmleditor.azurewebsites.net)

## Developer Guide

TODO

#### Profiles

- [Developer's Website](https://bjdelacruz.dev)
- [Coderbyte](https://coderbyte.com/profile/bjpeterdelacruz)
- [Codesignal](https://app.codesignal.com/profile/bjpeter)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)