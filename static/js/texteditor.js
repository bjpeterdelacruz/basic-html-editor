function loadHtml() {
    document.getElementById('html').textContent = document.getElementById('text-editor').innerHTML;
}

function format(command, value) {
    document.execCommand(command, false, value);
    loadHtml();
}

function showDialog(command, promptText) {
    const value = prompt(promptText);
    if (value) {
        document.execCommand(command, false, value);
    }
}

function clearHtml() {
    if (confirm('Are you sure you want to clear both the text editor and the HTML preview window?')) {
        document.getElementById('text-editor').innerHTML = '';
        loadHtml();
    }
}

function copyToClipboard() {
    navigator.clipboard.writeText(document.getElementById('text-editor').innerHTML).then(() => {
        alert('Successfully copied HTML to clipboard!');
    }, (_) => {
        alert('Failed to copy HTML to clipboard!');
    });
}

(() => {
    document.getElementById('year').innerHTML = new Date().getFullYear();
    document.getElementById('text-editor').onblur = () => {
        loadHtml();
    };
    document.execCommand('styleWithCSS', false, true);
    loadHtml();
})();
