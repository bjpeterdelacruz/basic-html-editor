DOCKER=docker

all: build_and_run

stop_and_rm:
	if $(DOCKER) ps -a | grep -q basic-html-editor-app; \
	then $(DOCKER) stop basic-html-editor-app && $(DOCKER) rm basic-html-editor-app; \
	fi

build_and_run: stop_and_rm
	$(DOCKER) build -t basic-html-editor . && \
	  $(DOCKER) run -dit --name basic-html-editor-app -p 82:80 -e MODULE_NAME=app \
	    -e VARIABLE_NAME=app basic-html-editor

build:
	$(DOCKER) build -t basic-html-editor .
