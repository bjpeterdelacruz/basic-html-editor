FROM tecktron/python-waitress:latest
LABEL maintainer="bj.peter.delacruz@gmail.com"
LABEL version="1.0"

RUN pip install --upgrade pip

COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY static /app/static
COPY templates /app/templates
COPY app.py /app/
